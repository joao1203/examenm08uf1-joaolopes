package com.example.examenm08uf1_joaolopes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.examenm08uf1_joaolopes.databinding.FragmentMenuBinding
import com.example.examenm08uf1_joaolopes.databinding.FragmentMenuBinding.inflate

class MenuFragment : Fragment() {

    lateinit var binding: FragmentMenuBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.startGame.setOnClickListener {
            val action = MenuFragmentDirections.actionMenuFragmentToGameFragment()
            findNavController().navigate(action)
        }
    }
}