package com.example.examenm08uf1_joaolopes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.examenm08uf1_joaolopes.databinding.FragmentResultBinding

class ResultFragment : Fragment() {

    lateinit var binding: FragmentResultBinding
    var cScore = 0
    var pScore = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentResultBinding.inflate(inflater, container, false)

        cScore = arguments?.getInt("cpuScore")!!
        pScore = arguments?.getInt("playerScore")!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.gameScore.text = "$pScore - $cScore"

        if (pScore > cScore){
            binding.textResult.text = "YOU WON!"
        }
        else if (pScore < cScore){
            binding.textResult.text = "YOU LOST!"
        }
        else{
            binding.textResult.text = "IT'S A TIE!"
        }

        binding.backMenu.setOnClickListener {
            val action = ResultFragmentDirections.actionResultFragmentToMenuFragment()
            findNavController().navigate(action)
        }
    }

}