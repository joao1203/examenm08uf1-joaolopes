package com.example.examenm08uf1_joaolopes

import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.examenm08uf1_joaolopes.databinding.FragmentGameBinding

class GameFragment : Fragment() {

    lateinit var binding: FragmentGameBinding
    lateinit var timer: CountDownTimer

    val handler = Handler(Looper.getMainLooper())
    val imageList = listOf<Int>(R.drawable.piedra,R.drawable.papel,R.drawable.tijera)
    var position: Int = 0
    var count: Int = 3
    var matches: Int = 0
    var cpuTry: Int = 0
    var playerScore: Int = 0
    var cpuScore: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentGameBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        gameCountDown()

        binding.playerAction.setOnClickListener {
            position++
            if (position == 3) position = 0
            binding.playerAction.setImageResource(imageList[position])
        }

        binding.nextRound.setOnClickListener {
            whenNext()
            gameCountDown()
        }


    }

    fun gameCountDown (){
        handler.postDelayed({
            timer = object : CountDownTimer(3000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    count--
                    binding.countdown.text = count.toString()
                }
                override fun onFinish(){
                    cpuTry = (0..2).random()
                    binding.cpuAction.setImageResource(imageList[cpuTry])
                    binding.playerAction.isEnabled = false
                    binding.countdown.visibility = View.GONE
                    binding.cpuAction.visibility = View.VISIBLE
                    binding.nextRound.visibility = View.VISIBLE

                    matchResult()

                    binding.gameScore.text = "$playerScore - $cpuScore"
                    matches++
                    if (matches == 3){
                        matches = 0
                        binding.nextRound.visibility = View.GONE
                        handler.postDelayed({
                            val action = GameFragmentDirections.actionGameFragmentToResultFragment(playerScore,cpuScore)
                            findNavController().navigate(action)
                            }, 1500)
                    }
                }
            }.start()
        }, 1000)
    }

    fun whenNext(){
        count = 3
        binding.countdown.text = count.toString()
        binding.nextRound.visibility = View.GONE
        binding.countdown.visibility = View.VISIBLE
        binding.cpuAction.visibility = View.GONE
        binding.playerAction.isEnabled = true

    }

    fun matchResult () {
        if (cpuTry == 0 && position == 1){
            playerScore++
        }
        else if (cpuTry == 1 && position == 2){
            playerScore++
        }
        else if (cpuTry == 2 && position == 0){
            playerScore++
        }
        else if (cpuTry == 0 && position == 2){
            cpuScore++
        }
        else if (cpuTry == 1 && position == 0){
            cpuScore++
        }
        else if (cpuTry == 2 && position == 1){
            cpuScore++
        }
    }
}